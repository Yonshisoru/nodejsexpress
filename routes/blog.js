var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');
const db = require('monk')('localhost:27017/testdb');
/* GET home page. */

//const url = 'localhost:27017/testdb';
//const db = monk(url);
db.then(() => {
  console.log('Connected correctly to server')
})


router.get('/', function(req, res, next) {
    res.render('blog',{title:'blog'});
  });
  router.get('/add', function(req, res, next) {
    res.render('addblog');
  });
  router.post('/add',[
    // username must be an email
    check("name","กรุณากรอกชื่อบทความ").not().isEmpty(),
    check("description","กรุณากรอกบทความ").not().isEmpty(),
    check("author","กรุณากรอกชื่อผู้แต่ง").not().isEmpty()
  ], function(req, res, next) {
    const result = validationResult(req);
    var errors = result.errors;
    if (!result.isEmpty()) {
      res.render('addblog',{errors:errors});
    }else{
      //insert to db
      var blog = db.get('test');
      blog.insert({
        name:req.body.name,
        description:req.body.description,
        author:req.body.author
      },function(err,error){
        if(err){
          res.send(err);
        }else{
          req.flash("success","บันทึกข้อความสำเร็จ");
          res.location('/blog/add');
          res.redirect('/blog/add');
        }
      })
    }
  });
  module.exports = router;